﻿using System.Collections.Generic;
using BookstoreMvcProject.Models;

namespace BookstoreMvcProject.Interfaces
{
	interface IBookRepo
	{
		IEnumerable<Book> ReadAllByBookstore(int bookstoreId);
		IEnumerable<Book> ReadAll();
		Book ReadById(int id);
		bool Create(Book book, int bookstoreId);
		void Update(Book book, int selectedGenreId);
		void Delete(int id);
		IEnumerable<Book> Search(string searchQuery, int searchCategory);
		IEnumerable<Book> Sort(int sortBy, int order);
	}
}
