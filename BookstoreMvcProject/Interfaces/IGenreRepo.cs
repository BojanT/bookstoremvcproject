﻿using System.Collections.Generic;
using BookstoreMvcProject.Models;

namespace BookstoreMvcProject.Interfaces
{
	interface IGenreRepo
	{
		IEnumerable<Genre> ReadAll();
		Genre ReadById(int id);
		bool Create(Genre genre);
		void Update(Genre genre);
		void Delete(int id);
	}
}
