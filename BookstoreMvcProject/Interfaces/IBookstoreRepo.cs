﻿using System.Collections.Generic;
using BookstoreMvcProject.Models;

namespace BookstoreMvcProject.Interfaces
{
	interface IBookstoreRepo
	{
		IEnumerable<Bookstore> ReadAll();
		Bookstore ReadById(int id);
		bool Create(Bookstore newBookstore);
		void Update(Bookstore bookstore);
		void Delete(int id);
	}
}
