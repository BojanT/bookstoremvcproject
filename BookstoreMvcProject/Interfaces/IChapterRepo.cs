﻿using System.Collections.Generic;
using BookstoreMvcProject.Models;

namespace BookstoreMvcProject.Interfaces
{
	interface IChapterRepo
	{
		IEnumerable<Chapter> ReadAllForBook(int bookId);
		IEnumerable<Chapter> ReadAll();
		Chapter ReadById(int id);
		bool Create(Chapter chapter);
		void AddChapterToBook(int chapterId, int bookId);
		void Update(Chapter chapter);
		void Delete(int id);
		void DeleteChapterFromBook(int chapterId, int bookId);
	}
}
