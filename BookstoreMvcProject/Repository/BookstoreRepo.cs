﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using BookstoreMvcProject.Interfaces;
using BookstoreMvcProject.Models;

namespace BookstoreMvcProject.Repository
{
	public class BookstoreRepo : IBookstoreRepo
	{
		private SqlConnection conn;
		private void Connection()
		{
			string connString = ConfigurationManager.ConnectionStrings["BookstoreDb"].ToString();
			conn = new SqlConnection(connString);
		}

		public IEnumerable<Bookstore> ReadAll()
		{
			try
			{
				string queryString = "SELECT * FROM Bookstores;";
				Connection();

				DataSet ds = new DataSet();
				DataTable dt = new DataTable();

				using (SqlCommand cmd = conn.CreateCommand())
				{
					cmd.CommandText = queryString;

					SqlDataAdapter sda = new SqlDataAdapter();
					sda.SelectCommand = cmd;

					sda.Fill(ds, "Bookstores");
					dt = ds.Tables["Bookstores"];
					conn.Close();
				}

				List<Bookstore> allBookstores = new List<Bookstore>();

				foreach (DataRow dataRow in dt.Rows)
				{
					int bookstoreId = int.Parse(dataRow["Bookstore_Id"].ToString());
					string bookstoreName = dataRow["Bookstore_Name"].ToString();
					allBookstores.Add(new Bookstore(bookstoreId, bookstoreName));
				}
				return allBookstores;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public Bookstore ReadById(int id)
		{
			try
			{
				string queryString = "SELECT * FROM Bookstores WHERE Bookstore_Id = @id;";
				Connection();

				DataSet ds = new DataSet();
				DataTable dt = new DataTable();

				using (SqlCommand cmd = conn.CreateCommand())
				{
					cmd.CommandText = queryString;
					cmd.Parameters.AddWithValue("@id", id);

					SqlDataAdapter sda = new SqlDataAdapter();
					sda.SelectCommand = cmd;

					sda.Fill(ds, "Bookstores");
					dt = ds.Tables["Bookstores"];
					conn.Close();
				}

				if (dt.Rows.Count != 0)
				{
					int bookstoreId = int.Parse(dt.Rows[0]["Bookstore_Id"].ToString());
					string bookstoreName = dt.Rows[0]["Bookstore_Name"].ToString();

					return new Bookstore(bookstoreId, bookstoreName);
				}
				return null;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public bool Create(Bookstore newBookstore)
		{
			try
			{
				string queryString = "INSERT INTO Bookstores (Bookstore_Name) VALUES (@bookstoreName);";
				queryString += "SELECT SCOPE_IDENTITY();";
				Connection();

				using (SqlCommand cmd = conn.CreateCommand())
				{
					cmd.CommandText = queryString;
					cmd.Parameters.AddWithValue("@bookstoreName", newBookstore.Name);

					conn.Open();
					var newId = cmd.ExecuteScalar();
					conn.Close();

					if (newId != null)
					{
						return true;
					}
				}
				return false;

			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public void Update(Bookstore bookstore)
		{
			try
			{
				string queryString = "UPDATE Bookstores SET Bookstore_Name = @BookstoreName WHERE Bookstore_Id = @BookstoreId;";
				Connection();

				using (SqlCommand cmd = conn.CreateCommand())
				{
					cmd.CommandText = queryString;
					cmd.Parameters.AddWithValue("@BookstoreId", bookstore.Id);
					cmd.Parameters.AddWithValue("@BookstoreName", bookstore.Name);

					conn.Open();
					cmd.ExecuteScalar();
					conn.Close();
				}
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public void Delete(int id)
		{
			try
			{
				string queryString = "DELETE FROM Bookstores WHERE Bookstore_Id = @BookstoreId;";
				Connection();

				using (SqlCommand cmd = conn.CreateCommand())
				{
					cmd.CommandText = queryString;
					cmd.Parameters.AddWithValue("@BookstoreId", id);

					conn.Open();
					cmd.ExecuteNonQuery();
					conn.Close();
				}
			}
			catch (Exception e)
			{
				throw e;
			}
		}
	}
}