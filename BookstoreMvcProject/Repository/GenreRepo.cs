﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using BookstoreMvcProject.Interfaces;
using BookstoreMvcProject.Models;

namespace BookstoreMvcProject.Repository
{
	public class GenreRepo : IGenreRepo
	{
		private SqlConnection conn;
		private void Connection()
		{
			string connString = ConfigurationManager.ConnectionStrings["BookstoreDb"].ToString();
			conn = new SqlConnection(connString);
		}

		public bool Create(Genre newGenre)
		{
			try
			{
				string queryString = "INSERT INTO Genres (Genre_Name) VALUES (@genreName);";
				queryString += "SELECT SCOPE_IDENTITY();";
				Connection();

				using (SqlCommand cmd = conn.CreateCommand())
				{
					cmd.CommandText = queryString;
					cmd.Parameters.AddWithValue("@genreName", newGenre.Name);

					conn.Open();
					var newId = cmd.ExecuteScalar();
					conn.Close();

					if (newId != null)
					{
						return true;
					}
				}
				return false;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public IEnumerable<Genre> ReadAll()
		{
			try
			{
				string queryString = "SELECT * FROM Genres";
				Connection();

				DataSet ds = new DataSet();
				DataTable dt = new DataTable();

				using (SqlCommand cmd = conn.CreateCommand())
				{
					cmd.CommandText = queryString;

					SqlDataAdapter sda = new SqlDataAdapter();
					sda.SelectCommand = cmd;

					sda.Fill(ds, "Genres");
					dt = ds.Tables["Genres"];
					conn.Close();
				}

				List<Genre> allGenres = new List<Genre>();

				foreach (DataRow dataRow in dt.Rows)
				{
					int genreId = int.Parse(dataRow["Genre_Id"].ToString());
					string genreName = dataRow["Genre_Name"].ToString();

					allGenres.Add(new Genre(genreId, genreName));
				}
				return allGenres;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public Genre ReadById(int id)
		{
			try
			{
				string queryString = "SELECT * FROM Genres WHERE Genre_Id = @genreId;";
				Connection();

				DataSet ds = new DataSet();
				DataTable dt = new DataTable();

				using (SqlCommand cmd = conn.CreateCommand())
				{
					cmd.CommandText = queryString;
					cmd.Parameters.AddWithValue("@genreId", id);

					SqlDataAdapter sda = new SqlDataAdapter();
					sda.SelectCommand = cmd;

					sda.Fill(ds, "Genres");
					dt = ds.Tables["Genres"];
					conn.Close();
				}

				if (dt.Rows.Count != 0)
				{
					int genreId = int.Parse(dt.Rows[0]["Genre_Id"].ToString());
					string genreName = dt.Rows[0]["Genre_Name"].ToString();

					return new Genre(genreId, genreName);
				}
				return null;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public void Update(Genre genre)
		{
			try
			{
				string queryString = "UPDATE Genres SET Genre_Name = @genreName WHERE Genre_Id = @genreId;";
				Connection();

				using (SqlCommand cmd = conn.CreateCommand())
				{
					cmd.CommandText = queryString;
					cmd.Parameters.AddWithValue("@genreName", genre.Name);
					cmd.Parameters.AddWithValue("@genreId", genre.Id);

					conn.Open();
					cmd.ExecuteNonQuery();
					conn.Close();
				}
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public void Delete(int id)
		{
			try
			{
				string queryString = "DELETE FROM Genres WHERE Genre_Id = @genreId;";
				Connection();

				using (SqlCommand cmd = conn.CreateCommand())
				{
					cmd.CommandText = queryString;
					cmd.Parameters.AddWithValue("@genreId", id);

					conn.Open();
					cmd.ExecuteNonQuery();
					conn.Close();
				}
			}
			catch (Exception e)
			{
				throw e;
			}
		}
	}
}