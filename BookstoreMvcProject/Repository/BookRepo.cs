﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using BookstoreMvcProject.Models;
using BookstoreMvcProject.Interfaces;

namespace BookstoreMvcProject.Repository
{
	public class BookRepo : IBookRepo
	{
		private SqlConnection conn;
		private void Connection()
		{
			string connString = ConfigurationManager.ConnectionStrings["BookstoreDb"].ToString();
			conn = new SqlConnection(connString);
		}

		public IEnumerable<Book> ReadAll()
		{
			try
			{
				string queryString = "SELECT * FROM Books;";
				Connection();

				DataSet ds = new DataSet();
				DataTable dt = new DataTable();

				using (SqlCommand cmd = conn.CreateCommand())
				{
					cmd.CommandText = queryString;

					SqlDataAdapter sda = new SqlDataAdapter();
					sda.SelectCommand = cmd;

					sda.Fill(ds, "Books");
					dt = ds.Tables["Books"];
					conn.Close();
				}

				List<Book> books = new List<Book>();

				foreach (DataRow dataRow in dt.Rows)
				{
					int bookId = int.Parse(dataRow["Book_Id"].ToString());
					string bookTitle = dataRow["Book_Title"].ToString();
					decimal bookPrice = decimal.Parse(dataRow["Book_Price"].ToString());

					books.Add(new Book(bookId, bookTitle, bookPrice, new Genre(), new Bookstore()));
				}
				return books;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public IEnumerable<Book> ReadAllByBookstore(int bookstoreId)
		{
			try
			{
				string queryString = "SELECT * FROM Books b INNER JOIN Genres g ON b.Genre_id = g.Genre_Id WHERE Bookstore_Id = @bookstoreId;";
				Connection();

				DataSet ds = new DataSet();
				DataTable dt = new DataTable();

				using (SqlCommand cmd = conn.CreateCommand())
				{
					cmd.CommandText = queryString;
					cmd.Parameters.AddWithValue("@bookstoreId", bookstoreId);

					SqlDataAdapter sda = new SqlDataAdapter();
					sda.SelectCommand = cmd;

					sda.Fill(ds, "Books");
					dt = ds.Tables["Books"];
					conn.Close();
				}

				List<Book> books = new List<Book>();

				foreach (DataRow dataRow in dt.Rows)
				{
					int bookId = int.Parse(dataRow["Book_Id"].ToString());
					string bookTitle = dataRow["Book_Title"].ToString();
					decimal bookPrice = decimal.Parse(dataRow["Book_Price"].ToString());

					int genreId = int.Parse(dataRow["Genre_Id"].ToString());
					string genreName = dataRow["Genre_Name"].ToString();

					books.Add(new Book(bookId, bookTitle, bookPrice, new Genre(genreId, genreName), new Bookstore()));
				}
				return books;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public Book ReadById(int id)
		{
			try
			{
				string queryString = "SELECT * FROM Books b INNER JOIN Genres g ON b.Genre_Id = g.Genre_Id WHERE Book_Id = @bookId;";
				Connection();

				DataSet ds = new DataSet();
				DataTable dt = new DataTable();

				using (SqlCommand cmd = conn.CreateCommand())
				{
					cmd.CommandText = queryString;
					cmd.Parameters.AddWithValue("@BookId", id);

					SqlDataAdapter sda = new SqlDataAdapter();
					sda.SelectCommand = cmd;

					sda.Fill(ds, "Books");
					dt = ds.Tables["Books"];
					conn.Close();
				}

				if (dt.Rows.Count != 0)
				{
					int bookId = int.Parse(dt.Rows[0]["Book_Id"].ToString());
					string bookTitle = dt.Rows[0]["Book_Title"].ToString();
					decimal bookPrice = decimal.Parse(dt.Rows[0]["Book_Price"].ToString());

					int genreId = int.Parse(dt.Rows[0]["Genre_Id"].ToString());
					string genreName = dt.Rows[0]["Genre_Name"].ToString();

					return new Book(bookId, bookTitle, bookPrice, new Genre(genreId, genreName), new Bookstore());
				}
				return null;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public bool Create(Book newBook, int bookstoreId)
		{
			try
			{
				string queryString = "INSERT INTO Books (Book_Title, Book_Price, Genre_Id, Bookstore_Id) VALUES (@bookTitle, @bookPrice, @genreId, @bookstoreId);";
				queryString += "SELECT SCOPE_IDENTITY();";
				Connection();

				using (SqlCommand cmd = conn.CreateCommand())
				{
					cmd.CommandText = queryString;
					cmd.Parameters.AddWithValue("@bookTitle", newBook.Title);
					cmd.Parameters.AddWithValue("@bookPrice", newBook.Price);
					cmd.Parameters.AddWithValue("@genreId", newBook.Genre.Id);
					cmd.Parameters.AddWithValue("@bookstoreId", bookstoreId);

					conn.Open();
					var newId = cmd.ExecuteScalar();
					conn.Close();

					if (newId != null)
					{
						return true;
					}
					return false;
				}
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public void Update(Book book, int selectedGenreId)
		{
			try
			{
				string queryString = "UPDATE Books SET Book_Title = @bookTitle, Book_Price = @bookPrice, Genre_Id = @genreId WHERE Book_Id = @bookId;";
				Connection();

				using (SqlCommand cmd = conn.CreateCommand())
				{
					cmd.CommandText = queryString;
					cmd.Parameters.AddWithValue("@bookTitle", book.Title);
					cmd.Parameters.AddWithValue("@bookPrice", book.Price);
					cmd.Parameters.AddWithValue("@genreId", selectedGenreId);
					cmd.Parameters.AddWithValue("@bookId", book.Id);

					conn.Open();
					cmd.ExecuteNonQuery();
					conn.Close();
				}
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public void Delete(int id)
		{
			try
			{
				string queryString = "DELETE FROM Books WHERE Book_Id = @bookId;";
				Connection();

				using (SqlCommand cmd = conn.CreateCommand())
				{
					cmd.CommandText = queryString;
					cmd.Parameters.AddWithValue("@bookId", id);

					conn.Open();
					cmd.ExecuteNonQuery();
					conn.Close();
				}
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public IEnumerable<Book> Search(string searchQuery, int searchCategory)
		{
			try
			{
				string queryString;

				DataSet ds = new DataSet();
				DataTable dt = new DataTable();

				Connection();

				List<Book> books = new List<Book>();

				if (searchCategory == 1) // search by book title
				{
					queryString = "SELECT DISTINCT * FROM Books WHERE Book_Title LIKE @searchQuery";
				}
				else if (searchCategory == 2) // search by genre
				{
					queryString = "SELECT DISTINCT b.Book_Id, b.Book_Title, b.Book_Price FROM Books b INNER JOIN Genres g ON b.Genre_Id = g.Genre_Id WHERE g.Genre_Name LIKE @searchQuery;";
				}
				else // search by chapter
				{
					queryString = "SELECT DISTINCT b.Book_Id, b.Book_Title, b.Book_Price FROM Books b INNER JOIN BooksChapters bc ON b.Book_Id = bc.Book_Id INNER JOIN Chapters c ON bc.Chapter_Id = c.Chapter_Id WHERE c.Chapter_Name LIKE @searchQuery;";
				}

				using (SqlCommand cmd = conn.CreateCommand())
				{
					cmd.CommandText = queryString;
					cmd.Parameters.AddWithValue("@searchQuery", "%" + searchQuery + "%");

					SqlDataAdapter sda = new SqlDataAdapter();
					sda.SelectCommand = cmd;

					sda.Fill(ds, "Books");
					dt = ds.Tables["Books"];
					conn.Close();
				}

				foreach (DataRow dataRow in dt.Rows)
				{
					int bookId = int.Parse(dataRow["Book_Id"].ToString());
					string bookTitle = dataRow["Book_Title"].ToString();
					decimal bookPrice = decimal.Parse(dataRow["Book_Price"].ToString());

					books.Add(new Book() { Id = bookId, Title = bookTitle, Price = bookPrice });
				}
				return books;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public IEnumerable<Book> Sort(int sortBy, int order)
		{
			try
			{
				string queryString;
				Connection();

				DataSet ds = new DataSet();
				DataTable dt = new DataTable();

				List<Book> sortedBooks = new List<Book>();

				if (sortBy == 1 && order == 1) // sort books by title ascending
				{
					queryString = "SELECT * FROM Books ORDER BY Book_Title ASC;";
				}
				else if (sortBy == 1 && order == 2) // sort books by title descending
				{
					queryString = "SELECT * FROM Books ORDER BY Book_Title DESC;";
				}
				else if (sortBy == 2 && order == 1) // sort books by price ascending
				{
					queryString = "SELECT * FROM Books ORDER BY Book_Price ASC;";
				}
				else // sort books by price descending
				{
					queryString = "SELECT * FROM Books ORDER BY Book_Price DESC;";
				}

				using (SqlCommand cmd = conn.CreateCommand())
				{
					cmd.CommandText = queryString;

					SqlDataAdapter sda = new SqlDataAdapter();
					sda.SelectCommand = cmd;

					sda.Fill(ds, "Books");
					dt = ds.Tables["Books"];
					conn.Close();
				}

				foreach (DataRow dataRow in dt.Rows)
				{
					int bookId = int.Parse(dataRow["Book_Id"].ToString());
					string bookTitle = dataRow["Book_Title"].ToString();
					decimal bookPrice = decimal.Parse(dataRow["Book_Price"].ToString());

					sortedBooks.Add(new Book() { Id = bookId, Title = bookTitle, Price = bookPrice });
				}
				return sortedBooks;
			}
			catch (Exception e)
			{
				throw e;
			}
		}
	}
}