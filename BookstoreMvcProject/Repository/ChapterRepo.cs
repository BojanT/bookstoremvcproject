﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using BookstoreMvcProject.Interfaces;
using BookstoreMvcProject.Models;

namespace BookstoreMvcProject.Repository
{
	public class ChapterRepo : IChapterRepo
	{
		private SqlConnection conn;
		private void Connection()
		{
			string connString = ConfigurationManager.ConnectionStrings["BookstoreDb"].ToString();
			conn = new SqlConnection(connString);
		}

		public bool Create(Chapter newChapter)
		{
			try
			{
				string queryString = "INSERT INTO Chapters (Chapter_Name) VALUES (@chapterName);";
				queryString += "SELECT SCOPE_IDENTITY();";
				Connection();

				using (SqlCommand cmd = conn.CreateCommand())
				{
					cmd.CommandText = queryString;
					cmd.Parameters.AddWithValue("@chapterName", newChapter.Name);

					conn.Open();
					var newId = cmd.ExecuteScalar();
					conn.Close();

					if (newId != null)
					{
						return true;
					}
					return false;
				}
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public IEnumerable<Chapter> ReadAllForBook(int bookId)
		{
			try
			{
				string queryString = "SELECT * FROM Chapters c INNER JOIN BooksChapters bc ON c.Chapter_Id = bc.Chapter_Id WHERE Book_Id = @bookId;";
				Connection();

				DataSet ds = new DataSet();
				DataTable dt = new DataTable();

				using (SqlCommand cmd = conn.CreateCommand())
				{
					cmd.CommandText = queryString;
					cmd.Parameters.AddWithValue("@bookId", bookId);

					SqlDataAdapter sda = new SqlDataAdapter();
					sda.SelectCommand = cmd;

					sda.Fill(ds, "Chapters");
					dt = ds.Tables["Chapters"];
					conn.Close();
				}

				List<Chapter> chapters = new List<Chapter>();

				foreach (DataRow dataRow in dt.Rows)
				{
					int chapterId = int.Parse(dataRow["Chapter_Id"].ToString());
					string chapterName = dataRow["Chapter_Name"].ToString();

					chapters.Add(new Chapter(chapterId, chapterName));
				}
				return chapters;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public IEnumerable<Chapter> ReadAll()
		{
			try
			{
				string queryString = "SELECT * FROM Chapters";
				Connection();

				DataSet ds = new DataSet();
				DataTable dt = new DataTable();

				using (SqlCommand cmd = conn.CreateCommand())
				{
					cmd.CommandText = queryString;

					SqlDataAdapter sda = new SqlDataAdapter();
					sda.SelectCommand = cmd;

					sda.Fill(ds, "Chapters");
					dt = ds.Tables["Chapters"];
					conn.Open();
				}

				List<Chapter> allChapters = new List<Chapter>();

				foreach (DataRow dataRow in dt.Rows)
				{
					int chapterId = int.Parse(dataRow["Chapter_Id"].ToString());
					string chapterName = dataRow["Chapter_Name"].ToString();

					allChapters.Add(new Chapter(chapterId, chapterName));
				}
				return allChapters;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public Chapter ReadById(int id)
		{
			try
			{
				string queryString = "SELECT * FROM Chapters WHERE Chapter_Id = @chapterId;";
				Connection();

				DataSet ds = new DataSet();
				DataTable dt = new DataTable();

				using (SqlCommand cmd = conn.CreateCommand())
				{
					cmd.CommandText = queryString;
					cmd.Parameters.AddWithValue("@chapterId", id);

					SqlDataAdapter sda = new SqlDataAdapter();
					sda.SelectCommand = cmd;

					sda.Fill(ds, "Chapters");
					dt = ds.Tables["Chapters"];
					conn.Close();
				}

				if (dt.Rows.Count != 0)
				{
					int chapterId = int.Parse(dt.Rows[0]["Chapter_Id"].ToString());
					string chapterName = dt.Rows[0]["Chapter_Name"].ToString();

					return new Chapter(chapterId, chapterName);
				}
				return null;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public void AddChapterToBook(int bookId, int chapterId)
		{
			try
			{
				string queryString = "INSERT INTO BooksChapters (Book_Id, Chapter_Id) VALUES (@bookId, @chapterId);";
				Connection();

				using (SqlCommand cmd = conn.CreateCommand())
				{
					cmd.CommandText = queryString;
					cmd.Parameters.AddWithValue("@chapterId", chapterId);
					cmd.Parameters.AddWithValue("@bookId", bookId);

					conn.Open();
					cmd.ExecuteNonQuery();
					conn.Close();
				}
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public void Update(Chapter chapter)
		{
			try
			{
				string queryString = "UPDATE Chapters SET Chapter_Name = @chapterName WHERE Chapter_Id = @chapterId";
				Connection();

				using (SqlCommand cmd = conn.CreateCommand())
				{
					cmd.CommandText = queryString;
					cmd.Parameters.AddWithValue("@chapterName", chapter.Name);
					cmd.Parameters.AddWithValue("@chapterId", chapter.Id);

					conn.Open();
					cmd.ExecuteNonQuery();
					conn.Close();
				}
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public void DeleteChapterFromBook(int bookId, int chapterId)
		{
			try
			{
				string queryString = "DELETE FROM BooksChapters WHERE Book_Id = @bookId AND Chapter_Id = chapterId;";
				Connection();

				using (SqlCommand cmd = conn.CreateCommand())
				{
					cmd.CommandText = queryString;
					cmd.Parameters.AddWithValue("@bookId", bookId);
					cmd.Parameters.AddWithValue("@chapterId", chapterId);

					conn.Open();
					cmd.ExecuteNonQuery();
					conn.Close();
				}
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public void Delete(int id)
		{
			try
			{
				string queryString = "DELETE FROM Chapters WHERE Chapter_Id = @chapterId;";
				Connection();

				using (SqlCommand cmd = conn.CreateCommand())
				{
					cmd.CommandText = queryString;
					cmd.Parameters.AddWithValue("@chapterId", id);

					conn.Open();
					cmd.ExecuteNonQuery();
					conn.Close();
				}
			}
			catch (Exception e)
			{
				throw e;
			}
		}
	}
}