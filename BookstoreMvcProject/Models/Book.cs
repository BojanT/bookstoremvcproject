﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BookstoreMvcProject.Models
{
	public class Book
	{
		public int Id { get; set; }

		[Required]
		[StringLength(80)]
		public string Title { get; set; }

		public List<Chapter> Chapters { get; set; }

		[Required]
		[Range(1, int.MaxValue)]
		public decimal Price { get; set; }

		public Genre Genre { get; set; }

		public Bookstore Bookstore { get; set; }

		public Book()
		{
			this.Chapters = new List<Chapter>();
		}

		public Book(int id, string title, decimal price, Genre genre, Bookstore bookstore)
		{
			this.Id = id;
			this.Title = title;
			this.Chapters = new List<Chapter>();
			this.Price = price;
			this.Genre = genre;
			this.Bookstore = bookstore;
		}
	}
}