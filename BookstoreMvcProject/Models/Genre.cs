﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BookstoreMvcProject.Models
{
	public class Genre
	{
		public int Id { get; set; }

		[Required]
		[StringLength(20)]
		public string Name { get; set; }

		public List<Book> Books { get; set; }

		public Genre()
		{
			this.Books = new List<Book>();
		}

		public Genre(int id, string name)
		{
			this.Id = id;
			this.Name = name;
			this.Books = new List<Book>();
		}
	}
}