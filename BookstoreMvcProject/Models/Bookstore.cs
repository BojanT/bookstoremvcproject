﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BookstoreMvcProject.Models
{
	public class Bookstore
	{
		public int Id { get; set; }

		[Required]
		[StringLength(80)]
		public string Name { get; set; }

		public List<Book> Books { get; set; }

		public Bookstore()
		{
			this.Books = new List<Book>();
		}

		public Bookstore(int id, string name)
		{
			this.Id = id;
			this.Name = name;
			this.Books = new List<Book>();
		}
	}
}