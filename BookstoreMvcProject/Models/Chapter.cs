﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BookstoreMvcProject.Models
{
	public class Chapter
	{
		public int Id { get; set; }

		[Required]
		[StringLength(80)]
		public string Name { get; set; }

		public List<Book> Books { get; set; }

		public Chapter()
		{
			this.Books = new List<Book>();
		}

		public Chapter(int id, string name)
		{
			this.Id = id;
			this.Name = name;
			this.Books = new List<Book>();
		}
	}
}