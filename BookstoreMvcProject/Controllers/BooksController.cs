﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BookstoreMvcProject.Models;
using BookstoreMvcProject.Interfaces;
using BookstoreMvcProject.Repository;
using BookstoreMvcProject.ViewModels;

namespace BookstoreMvcProject.Controllers
{
	[RoutePrefix("Knjiga")]
    public class BooksController : Controller
    {
		private IBookRepo bookRepo = new BookRepo();
		private IBookstoreRepo bookstoreRepo = new BookstoreRepo();
		private IGenreRepo genreRepo = new GenreRepo();
		private IChapterRepo chapterRepo = new ChapterRepo();

		[Route("")]
		public ActionResult Index()
		{
			List<Genre> genres = genreRepo.ReadAll().ToList();
			List<Bookstore> bookstores = bookstoreRepo.ReadAll().ToList();

			BookstoresGenresViewModel bgvm = new BookstoresGenresViewModel() { Genres = genres, Bookstores = bookstores };

			return View(bgvm);
		}

		[Route("Napravi")]
		public ActionResult Add(Book newBook, int genreId, int bookstoreId)
		{
			if (newBook == null)
			{
				return View("Error");
			}

			if (ModelState.IsValid)
			{
				Genre genre = genreRepo.ReadById(genreId);
				Bookstore bookstore = bookstoreRepo.ReadById(bookstoreId);

				if (genre != null && bookstore != null)
				{
					newBook.Bookstore = bookstore;
					newBook.Genre = genre;
					if (bookRepo.Create(newBook, bookstoreId))
					{
						return RedirectToAction("List", "Books");
					}
				}
			}
			return View("Error");
		}

		[Route("Izlistaj")]
		public ActionResult List(int? bookstoreId)
		{
			List<Bookstore> bookstores = bookstoreRepo.ReadAll().ToList();
			List<Book> books = new List<Book>();

			if (bookstoreId.HasValue)
			{
				books = bookRepo.ReadAllByBookstore(bookstoreId.Value).ToList();
			}

			BooksBookstoresViewModel bbvm = new BooksBookstoresViewModel() { Books = books, Bookstores = bookstores };

			return View(bbvm);
		}

		[Route("Azuriraj/{id:int}")]
		public ActionResult Update(int id)
		{
			List<Genre> genres = genreRepo.ReadAll().ToList();
			Book book = bookRepo.ReadById(id);
			List<Chapter> chapters = chapterRepo.ReadAllForBook(book.Id).ToList();

			book.Chapters = chapters;

			BookGenresViewModel bgvm = new BookGenresViewModel() { Book = book, Genres = genres, SelectedGenreId = book.Genre.Id };

			return View(bgvm);
		}

		[Route("Azuriraj")]
		[HttpPost]
		public ActionResult Update(Book book, int selectedGenreId)
		{
			if (book == null)
			{
				return View("Error");
			}

			if (ModelState.IsValid)
			{
				bookRepo.Update(book, selectedGenreId);

				return RedirectToAction("List");
			}
			return View("Error");
		}

		[Route("Obrisi/{id:int}")]
		public ActionResult Delete(int id)
		{
			bookRepo.Delete(id);

			return RedirectToAction("List");
		}

		[Route("ObrisiPoglavlje")]
		public ActionResult DeleteChapter(int bookId, int chapterId)
		{
			chapterRepo.DeleteChapterFromBook(bookId, chapterId);

			return RedirectToAction("List");
		}
	}
}