﻿using System.Web.Mvc;
using BookstoreMvcProject.Models;
using BookstoreMvcProject.Interfaces;
using BookstoreMvcProject.Repository;

namespace BookstoreMvcProject.Controllers
{
	[RoutePrefix("Knjizara")]
    public class BookstoresController : Controller
    {
		private IBookRepo bookRepo = new BookRepo();
		private IBookstoreRepo bookstoreRepo = new BookstoreRepo();

		[Route("")]
		public ActionResult Index()
		{
			return View();
		}

		[Route("Napravi")]
		public ActionResult Add(Bookstore bookstore)
		{
			if (bookstore == null)
			{
				return View("Error");
			}

			if (ModelState.IsValid)
			{
				if (bookstoreRepo.Create(bookstore))
				{
					return RedirectToAction("List");
				}
			}
			return View("Error");
		}

		[Route("Izlistaj")]
		public ActionResult List()
		{
			var bookstores = bookstoreRepo.ReadAll();

			return View(bookstores);
		}

		[Route("Azuriraj/{id:int}")]
		public ActionResult Update(int id)
		{
			var bookstore = bookstoreRepo.ReadById(id);

			return View(bookstore);
		}

		[Route("Azuriraj")]
		[HttpPost]
		public ActionResult Update(Bookstore bookstore)
		{
			if (bookstore == null)
			{
				return View("Error");
			}

			if (ModelState.IsValid)
			{
				bookstoreRepo.Update(bookstore);

				return RedirectToAction("List");
			}
			return View("Error");
		}

		[Route("Pretrazi")]
		public ActionResult Search()
		{
			return View();
		}

		public ActionResult SearchResults(string searchQuery, int searchCategory)
		{
			if (searchQuery == null)
			{
				return View("Error");
			}

			var books = bookRepo.Search(searchQuery, searchCategory);

			if (books != null)
			{
				return View(books);
			}
			return View("Error");
		}

		[Route("Sortiraj")]
		public ActionResult Sort()
		{
			return View();
		}

		public ActionResult SortResults(int sortBy, int order)
		{
			var books = bookRepo.Sort(sortBy, order);

			if (books != null)
			{
				return View(books);
			}
			return View("Error");
		}

		[Route("Obrisi/{id:int}")]
		public ActionResult Delete(int id)
		{
			bookstoreRepo.Delete(id);

			return RedirectToAction("List");
		}
	}
}