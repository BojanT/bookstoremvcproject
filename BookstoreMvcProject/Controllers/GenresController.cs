﻿using System.Web.Mvc;
using BookstoreMvcProject.Models;
using BookstoreMvcProject.Interfaces;
using BookstoreMvcProject.Repository;

namespace BookstoreMvcProject.Controllers
{
	[RoutePrefix("Zanr")]
    public class GenresController : Controller
    {
		private IGenreRepo genreRepo = new GenreRepo();

		[Route("")]
		public ActionResult Index()
		{
			return View();
		}

		[Route("Napravi")]
		public ActionResult Add(string name)
		{
			if (string.IsNullOrEmpty(name))
			{
				return View("Error");
			}

			if (ModelState.IsValid)
			{
				if (genreRepo.Create(new Genre() { Name = name }))
				{
					return RedirectToAction("List");
				}
			}
			return View("Error");
		}

		[Route("Izlistaj")]
		public ActionResult List()
		{
			var genres = genreRepo.ReadAll();

			return View(genres);
		}

		[Route("Azuriraj/{id:int}")]
		public ActionResult Update(int id)
		{
			var genre = genreRepo.ReadById(id);

			if (genre != null)
			{
				return View(genre);
			}
			return RedirectToAction("List");
		}

		[Route("Azuriraj")]
		[HttpPost]
		public ActionResult Update(Genre genre)
		{
			if (genre == null)
			{
				return View("Error");
			}

			if (ModelState.IsValid)
			{
				genreRepo.Update(genre);

				return RedirectToAction("List");
			}
			return View("Error");
		}

		[Route("Obrisi/{id:int}")]
		public ActionResult Delete(int id)
		{
			genreRepo.Delete(id);

			return RedirectToAction("List");
		}
	}
}