﻿using System.Linq;
using System.Web.Mvc;
using BookstoreMvcProject.Models;
using BookstoreMvcProject.Interfaces;
using BookstoreMvcProject.Repository;
using BookstoreMvcProject.ViewModels;

namespace BookstoreMvcProject.Controllers
{
	[RoutePrefix("Poglavlje")]
    public class ChaptersController : Controller
    {
		private IChapterRepo chapterRepo = new ChapterRepo();
		private IBookRepo bookRepo = new BookRepo();

		[Route("")]
		public ActionResult Index()
		{
			var books = bookRepo.ReadAll().ToList();
			var chapters = chapterRepo.ReadAll().ToList();

			BooksChaptersViewModel bcvm = new BooksChaptersViewModel() { Books = books, Chapters = chapters };

			return View(bcvm);
		}

		[Route("Napravi")]
		public ActionResult Add(string name)
		{
			if (string.IsNullOrEmpty(name))
			{
				return View("Error");
			}

			if (ModelState.IsValid)
			{
				if (chapterRepo.Create(new Chapter() { Name = name }))
				{
					return RedirectToAction("Index");
				}
			}
			return View("Error");
		}

		[Route("DodajPoglavljeKnjizi")]
		public ActionResult AddChapterToBook(int? bookId, int? chapterId)
		{
			if (bookId.HasValue && chapterId.HasValue)
			{
				chapterRepo.AddChapterToBook(bookId.Value, chapterId.Value);
			}
			return RedirectToAction("Index");
		}

		[Route("Azuriraj/{id:int}")]
		public ActionResult Update(int id)
		{
			var chapter = chapterRepo.ReadById(id);

			if (chapter != null)
			{
				return View(chapter);
			}
			return View("Index");
		}

		[Route("Azuriraj")]
		[HttpPost]
		public ActionResult Update(Chapter chapter)
		{
			if (chapter == null)
			{
				return View("Error");
			}

			if (ModelState.IsValid)
			{
				chapterRepo.Update(chapter);

				return RedirectToAction("Index");
			}
			return View("Error");
		}

		[Route("Delete/{id:int}")]
		public ActionResult Delete(int id)
		{
			chapterRepo.Delete(id);

			return RedirectToAction("Index");
		}
	}
}