﻿using System.Collections.Generic;
using BookstoreMvcProject.Models;

namespace BookstoreMvcProject.ViewModels
{
	public class BookGenresViewModel
	{
		public Book Book { get; set; }
		public List<Genre> Genres { get; set; }
		public int SelectedGenreId { get; set; }
	}
}