﻿using System.Collections.Generic;
using BookstoreMvcProject.Models;

namespace BookstoreMvcProject.ViewModels
{
	public class BookstoresGenresViewModel
	{
		public List<Bookstore> Bookstores { get; set; }
		public List<Genre> Genres { get; set; }
	}
}