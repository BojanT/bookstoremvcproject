﻿using System.Collections.Generic;
using BookstoreMvcProject.Models;

namespace BookstoreMvcProject.ViewModels
{
	public class BooksChaptersViewModel
	{
		public List<Book> Books { get; set; }
		public List<Chapter> Chapters { get; set; }
	}
}