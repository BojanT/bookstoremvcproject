﻿using System.Collections.Generic;
using BookstoreMvcProject.Models;

namespace BookstoreMvcProject.ViewModels
{
	public class BooksBookstoresViewModel
	{
		public List<Book> Books { get; set; }
		public List<Bookstore> Bookstores { get; set; }
		public int SelectedBookstoreId { get; set; }
	}
}